﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventPoints : MonoBehaviour {
    public Transform wheelTransformPoint;
    public Transform jacketTransformPoint;
    public Transform tvTransformPoint;
    public Transform bucketTransformPoint;
    public Transform xyloTransformPoint;

    public bool? isRightObject;

    bool isFilled = false;
    private void Start()
    {
        isRightObject = null;
    }
    // Update is called once per frame
    void Update () {
        
    }

    private void OnTriggerStay(Collider other)
    {
        //StartCoroutine(DellayTrigg(other));
        if (other.GetComponent<MouseDrag>().objectActive && !isFilled && other.GetComponent<MouseDrag>().drop)
        {
            if (transform.tag == other.tag)
            {
                isRightObject = true;
            }
            else
            {
                isRightObject = false;
            }
            //go.transform.SetParent(goWall.transform);
            Transform child = other.transform;
            if (other.tag == wheelTransformPoint.tag)
            {
                other.transform.SetParent(wheelTransformPoint);
            }
            if (other.tag == jacketTransformPoint.tag)
            {
                other.transform.SetParent(jacketTransformPoint);
            }
            if (other.tag == tvTransformPoint.tag)
            {
                other.transform.SetParent(tvTransformPoint);
            }
            if (other.tag == bucketTransformPoint.tag)
            {
                other.transform.SetParent(bucketTransformPoint);
            }
            if (other.tag == xyloTransformPoint.tag)
            {
                other.transform.SetParent(xyloTransformPoint);
                //child.localRotation = Quaternion.identity;
                //child.position = Vector3.zero;
                //other.transform.localScale = Vector3.one;
            }

        

            other.GetComponent<MouseDrag>().inPoint = true;

            other.gameObject.transform.position = wheelTransformPoint.position;
            other.gameObject.transform.rotation = wheelTransformPoint.rotation;
            other.gameObject.GetComponent<Rigidbody>().isKinematic = true;
            isFilled = true;

            child.localPosition = Vector3.zero;
            child.localRotation = new Quaternion(0, 0, 0, 0);
        }
    }

    //IEnumerator DellayTrigg(Collider other)
    //{
    //    if (other.GetComponent<MouseDrag>().objectActive && !isFilled && other.GetComponent<MouseDrag>().drop)
    //    {
    //        if (transform.name == other.name)
    //        {
    //            isRightObject = true;
    //        }
    //        else
    //        {
    //            isRightObject = false;
    //        }

    //        other.GetComponent<MouseDrag>().inPoint = true;

    //        other.gameObject.transform.position = wheelTransformPoint.position;
    //        other.gameObject.transform.rotation = wheelTransformPoint.rotation;
    //        other.gameObject.GetComponent<Rigidbody>().isKinematic = true;
    //        isFilled = true;
    //    }
    //    yield return new WaitForSeconds(0.1f);
    //}
}
