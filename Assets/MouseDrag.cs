﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseDrag : MonoBehaviour
{

    public float distance = 10;
    public bool objectActive = false;
    public bool inPoint = false;
    public bool drop = false;
    public Vector3 startPosition;
    public LayerMask m_LayerMask;
    private CheckLogic _checkLogic;
    public float overlapRadius = 0.2f;
    bool allowDrag = true;
    float timer;
    Camera mcam;
    private void Awake()
    {

        startPosition = transform.position;
    }

    private void Start()
    {
        objectActive = false;
        inPoint = false;
        drop = false;
        mcam = Camera.main;
        overlapRadius = 0.2f;
        allowDrag = true;
        timer = 0;
        _checkLogic = GameObject.FindObjectOfType<CheckLogic>();
    }
    private void Update()
    {
       OverlapPhisics();
    }
    private void OnMouseDrag()
    {
        Vector3 mousePos = new Vector3(Input.mousePosition.x, Input.mousePosition.y, distance);
        
        Vector3 objectPos = mcam.ScreenToWorldPoint(mousePos);
        Vector3 objectPosNegative = -mcam.ScreenToWorldPoint(mousePos);
        if (!inPoint && allowDrag)
        {
            
            
            //print(objectActive);
            transform.GetComponent<Rigidbody>().MovePosition(objectPos);
        }
        else
        {
            transform.GetComponent<Rigidbody>().MovePosition(new Vector3(objectPos.x, 2f, objectPos.z));
        }

    }
    void OverlapPhisics()
    {
        timer += Time.deltaTime;
        Collider[] hitColliders = Physics.OverlapSphere(transform.position, overlapRadius/3, m_LayerMask);
        

        if(hitColliders.Length > 0){
            print("Hit");
            allowDrag = false;
            objectActive = false;
            inPoint = false;
            drop = false;

        }
        else
        {
            print("not hit");
            if (timer > 1f)
            {
                allowDrag = true;
                timer = 0;
            }
            //allowDrag = true;
        }
        hitColliders = null;
    }
    private void OnMouseDown()
    {
        
        objectActive = true;
        _checkLogic.PointFor();
    }

    private void OnMouseOver()
    {

    }

    private void OnMouseUp()
    {
        
        if (objectActive)
        {
            drop = true;
        }

        print(drop);
        //objectActive = false;
 
        _checkLogic.PointForOff();
    }

    public void StayObj()
    {

    }
    void OnEnable()
    {
       // transform.localPosition = Vector3.zero;
    }
    void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        //Check that it is being run in Play Mode, so it doesn't try to draw this in Editor mode
        
            //Draw a cube where the OverlapBox is (positioned where your GameObject is as well as a size)
            Gizmos.DrawWireSphere(transform.position, overlapRadius/3);
    }
}