﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StalaktidControll : MonoBehaviour {

    public Transform waterSpawner;
    public Transform water;
    float timer = 0;
    Transform waterDrop;

    void Update ()
    {
        timer += Time.deltaTime;
        if (timer > 1f)
        {
            waterDrop = Instantiate(water, waterSpawner.position, Quaternion.identity);
            timer = 0;
            Destroy(waterDrop.gameObject, 3f);
        }
    }
}
