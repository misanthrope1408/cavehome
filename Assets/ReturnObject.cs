﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReturnObject : MonoBehaviour {
    public Transform portal;
    Animator anim;
    GameObject Temp;
    GameObject Temp2;
    bool allowTrigger = true;

    float timer = 0;
	// Use this for initialization
	void Start () {
        anim = portal.GetComponent<Animator>();
	}
	
	void Update () {
        timer += Time.deltaTime;
        if (timer >= 1f)
        {
            anim.SetBool("playPortal", false);
            allowTrigger = true;
            timer = 0;
        }
	}
    private void OnCollisionEnter(Collision collision)
    {
        if (allowTrigger)
        {
            Temp = collision.transform.gameObject;
            Temp.GetComponent<MouseDrag>().objectActive = false;
            Destroy(collision.transform.gameObject);
            anim.SetBool("playPortal", true);
            Temp2 = Instantiate(Temp, portal.position, Quaternion.identity);
            Temp2.GetComponent<MouseDrag>().enabled = true;
            Temp2.GetComponent<MouseDrag>().objectActive = false;
            Temp2.GetComponent<MouseDrag>().drop = false;

            allowTrigger = false;
        }
    }

}
