﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckLogic : MonoBehaviour {
    public Transform Events;
    public EventPoints[] CheckingPoints;
    public int validObject;
    public float timer;
    int isFun = 0;
    int isHate = 0;
    public Animator anim;

    public Transform[] points;

    void Start () {
        CheckingPoints = Events.GetComponentsInChildren<EventPoints>();
        timer = 0;
    }
    void FunWoman()
    {
        print("Iam fun");
        anim.SetBool("isWin", true);
        anim.SetBool("isNo", false);
    }
    void AngryWoman()
    {
        print("Iam angry");
        anim.SetBool("isNo", true);
    }

    void Update () {
        timer += Time.deltaTime;
        if (timer >= 3)
        {
            print("Tick");
            for (int i = 0; i < CheckingPoints.Length; i++)
            {
                if(CheckingPoints[i].isRightObject != null)
                {
                    if (CheckingPoints[i].isRightObject == true)
                    {
                        isFun++;
                        if (isFun == 5)
                        {
                            FunWoman();
                        }
                    }
                    else
                    {
                        isHate++;
                        if (isHate + isFun == 5)
                        {
                            AngryWoman();
                        }
                    }
                    
                }
            }
            print("Isfun"+isFun + " Hate" +isHate);
            isFun = 0;
            isHate = 0;
            timer = 0;
        }
    }

    public void PointFor()
    {
        for (int i = 0; i < points.Length; i++)
        {
            points[i].gameObject.SetActive(true);
        }
    }

    public void PointForOff()
    {
        for (int i = 0; i < points.Length; i++)
        {
            points[i].gameObject.SetActive(false);
        }
    }
}
